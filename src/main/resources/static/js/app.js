var vm = new Vue({
	el : '#app',
	data : {
		reccount : 0
	},
	methods : {
		onCreate : function() {
			this.$http.get("/list").then(response=>{
				this.reccount = response.body.length;
			}, response=>{
				console.log("err", response);
			});
		}
	},
	created : function() {
		this.onCreate();
	},
	template: '<span>Total:{{reccount}}</span>'
});
