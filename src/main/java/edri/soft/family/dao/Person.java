package edri.soft.family.dao;

import java.util.Date;
import java.util.Objects;

public class Person {

	private Long id;
	private String name;
	private String surname;
	private String gender;
	private Date birthday;
	private Date created;

	public Person() {
	}

	public Person(Long id, String name, String surname, String gender, Date birthday, Date created) {
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.gender = gender;
		this.birthday = birthday;
		this.created = created;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		if (this.id != null)
			hash = 29 * hash + Objects.hashCode(this.id);
		if (this.name != null)
			hash = 29 * hash + Objects.hashCode(this.name);
		if (this.surname != null)
			hash = 29 * hash + Objects.hashCode(this.surname);
		if (this.gender != null)
			hash = 29 * hash + Objects.hashCode(this.gender);
		if (this.birthday != null)
			hash = 29 * hash + Objects.hashCode(this.birthday);
		if (this.created != null)
			hash = 29 * hash + Objects.hashCode(this.created);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Person other = (Person) obj;
		if (!Objects.equals(this.birthday, other.birthday)) {
			return false;
		}
		if (!Objects.equals(this.created, other.created)) {
			return false;
		}
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		if (!Objects.equals(this.surname, other.surname)) {
			return false;
		}
		if (!Objects.equals(this.gender, other.gender)) {
			return false;
		}
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

}