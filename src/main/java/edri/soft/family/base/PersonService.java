package edri.soft.family.base;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import edri.soft.family.dao.Person;

@Service
public class PersonService implements IPersonService {

	private static Logger logger = LoggerFactory.getLogger(PersonService.class);
	@Autowired
	private JdbcTemplate jtm;

	private static String sql_select = "select * from persons";
	private static String sql_insert = "insert into persons (name, surname, gender, birthday) values(?,?,?,?)";
	private static String sql_delete = "delete from persons where id=?";

	@Override
	public List<Person> findAll() {
		List<Person> persons = jtm.query(sql_select, new BeanPropertyRowMapper<Person>(Person.class));
		return persons;
	}

	@Override
	public boolean insert(Person p) {
		logger.info("insert {}", p);
		return jtm.update(sql_insert, p.getName(), p.getSurname(), p.getGender(), p.getBirthday()) > 0;
	}

	@Override
	public boolean delete(Integer id) {
		logger.info("delete {}", id);
		return jtm.update(sql_delete, id) > 0;
	}
}