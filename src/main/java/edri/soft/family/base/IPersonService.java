package edri.soft.family.base;

import java.util.List;

import edri.soft.family.dao.Person;

public interface IPersonService {

	public List<Person> findAll();

	public boolean insert(Person p);

	public boolean delete(Integer id);
}