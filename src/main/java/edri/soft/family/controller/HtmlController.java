package edri.soft.family.controller;

import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edri.soft.family.base.IPersonService;
import edri.soft.family.dao.Person;

@Controller
public class HtmlController {

	public static Logger logger = LoggerFactory.getLogger(HtmlController.class);

	@Autowired
	private IPersonService personService;

	@GetMapping(value = {"/", "/index"})
	public String index() {
		return "index";
	}

	@GetMapping("/admin")
	public String admin() {
		return "admin";
	}

	@GetMapping("/user")
	public String user() {
		return "user";
	}

	@GetMapping("/about")
	public String about() {
		return "about";
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@GetMapping("/403")
	public String error403() {
		return "403";
	}
	@RequestMapping(value = {"list-persons"})
	public String listJsp(Model model) {
		model.addAttribute("persons", personService.findAll());
		return "list-persons";
	}

	@RequestMapping(value = "/add-person", method = RequestMethod.GET)
	public String addPersonsGet(Map<String, Object> model) {
		return "add-person";
	}

	@RequestMapping(value = "/del-person/{id}", method = RequestMethod.GET)
	public String delPersonsGet(@PathVariable("id") final int id) {
		personService.delete(id);
		return "redirect:/list-persons";
	}

	@RequestMapping(value = "/add-person", method = RequestMethod.POST)
	public String addPersonPost(@RequestParam("name") String name, @RequestParam("surname") String surname, @RequestParam("gender") String gender,
			@RequestParam("birthday") @DateTimeFormat(pattern = "yyyy-MM-dd") Date bd, Map<String, Object> model) {
		Person p = new Person();
		p.setName(name);
		p.setSurname(surname);
		p.setBirthday(bd);
		p.setGender(gender);
		personService.insert(p);
		return "redirect:/list-persons";
	}
}
