package edri.soft.family.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edri.soft.family.base.IPersonService;
import edri.soft.family.dao.Person;

@Controller
public class ApiController {

	public static Logger logger = LoggerFactory.getLogger(ApiController.class);

	@Autowired
	private IPersonService personService;

	@GetMapping(path = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Person> sayHello() {
		return personService.findAll();
	}
}
